import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QrCodeScannerComponent } from './qr-code-scanner.component';

// Angular Material imports
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
	declarations: [
		QrCodeScannerComponent
	],
	imports: [
		CommonModule,
		MatDialogModule,
		MatButtonModule,
		MatIconModule
	]
})
export class QrCodeScannerModule { }
