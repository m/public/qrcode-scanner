import { Component, Inject, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { MatDialogRef, MatDialogState } from '@angular/material/dialog';

// We use jsqr (v1.4.0 for our Angular 12 project). It's a very efficient QR Code decoding library
import jsQR from 'jsqr';

@Component({
	selector: 'app-qr-code-scanner',
	templateUrl: './qr-code-scanner.component.html',
	styleUrls: ['./qr-code-scanner.component.scss']
})
export class QrCodeScannerComponent implements AfterViewInit {
	@ViewChild('videoElement') videoElement: ElementRef;
	@ViewChild('canvasElement') canvasElement: ElementRef;

	scanning: boolean = true;
	decodedData: string;

	constructor (public dialogRef: MatDialogRef<QrCodeScannerComponent>) { }


	ngAfterViewInit() {
		this.startScannerManually();
	}



	startScannerManually() {

		if (!this.scanning) {
			return;
		}

		const video = this.videoElement.nativeElement;
		const canvas = this.canvasElement.nativeElement;

		// It's considered best practice to activate willReadFrequently.
		// It will force the use of a software accelerated 2D canvas (instead of a hardware accelerated one) and can save memory when calling getImageData() frequently like in our case.
		// See: https://html.spec.whatwg.org/multipage/canvas.html#dom-canvasrenderingcontext2dsettings-willreadfrequently
		const context = canvas.getContext('2d', { willReadFrequently: true });


		// Request access to the camera
		navigator.mediaDevices.getUserMedia({ video: { facingMode: { exact: 'environment' } } })
			.then((stream) => {
				video.srcObject = stream;
				video.play();

				// Wait for 'loadedmetadata' event to get the video dimensions
				// Otherwise, dimensions will be set to 0.
				video.addEventListener('loadedmetadata', () => {
					// Continuously capture frames and scan for QR codes
					requestAnimationFrame(() => this.scanQRCode(video, canvas, context));
				});
			})
			.catch((error) => {
				console.error('Error accessing camera:', error);
			});
	}

	scanQRCode(video: HTMLVideoElement, canvas: HTMLCanvasElement, context: CanvasRenderingContext2D) {

		if (this.dialogRef.getState() !== MatDialogState.OPEN) return;

		// Set canvas dimensions to match the video frame size
		const videoWidth = video.videoWidth;
		const videoHeight = video.videoHeight;
		canvas.width = videoWidth;
		canvas.height = videoHeight;

		// Draw the current video frame onto the canvas
		context.drawImage(video, 0, 0, videoWidth, videoHeight);

		// Extract the image data from the canvas
		const imageData = context.getImageData(0, 0, videoWidth, videoHeight);

		// I decided to use jsQR but decoding logic is business-related.
		const code = jsQR(imageData.data, imageData.width, imageData.height);

		if (code) {
			this.decodedData = code.data;
		}

		// Call the next frame recursively
		requestAnimationFrame(() => this.scanQRCode(video, canvas, context));
	}

	stopScanner() {
		const video = this.videoElement.nativeElement;
		const stream = video.srcObject as MediaStream;

		if (stream) {
			stream.getTracks().forEach((track) => track.stop());
			video.srcObject = null;
		}

		this.scanning = false;
	}

	leaveScanner(): void {
		this.stopScanner();
		this.dialogRef.close();
	}

}


/* import { Component, Inject, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MultiFormatReader, BarcodeFormat, DecodeHintType, RGBLuminanceSource, BinaryBitmap, HybridBinarizer } from '@zxing/library';

@Component({
	selector: 'app-qr-code-scanner',
	templateUrl: './qr-code-scanner.component.html',
	styleUrls: ['./qr-code-scanner.component.scss']
})
export class QrCodeScannerComponent implements AfterViewInit {
	@ViewChild('videoElement') videoElement: ElementRef;
	@ViewChild('canvasElement') canvasElement: ElementRef;

	scanning: boolean = true;
	decodedData: string;

	constructor (public dialogRef: MatDialogRef<QrCodeScannerComponent>) { }

	ngAfterViewInit() {
		this.startScanner();
	}

	startScanner() {
		if (!this.scanning) {
			return;
		}

		// Check if the Barcode Detection API is supported
		if ('BarcodeDetector' in window) {
			this.startScannerWithBarcodeAPI();
		} else {
			this.startScannerManually();
		}
	}

	startScannerWithBarcodeAPI() {
		const video = this.videoElement.nativeElement;
		const canvas = this.canvasElement.nativeElement;

		const context = canvas.getContext('2d', { willReadFrequently: true });

		navigator.mediaDevices
			.getUserMedia({ video: { facingMode: { exact: 'environment' } } })
			.then((stream) => {
				video.srcObject = stream;
				video.play();

				video.addEventListener('loadedmetadata', () => {
					requestAnimationFrame(() => this.scanQRCodeWithBarcodeAPI(video, canvas, context));
				});
			})
			.catch((error) => {
				console.error('Error accessing camera:', error);
			});
	}

	scanQRCodeWithBarcodeAPI(video: HTMLVideoElement, canvas: HTMLCanvasElement, context: CanvasRenderingContext2D) {
		const videoWidth = video.videoWidth;
		const videoHeight = video.videoHeight;
		canvas.width = videoWidth;
		canvas.height = videoHeight;

		context.drawImage(video, 0, 0, videoWidth, videoHeight);

		const barcodeDetector = new BarcodeDetector();
		barcodeDetector
			.detect(canvas)
			.then((barcodes) => {
				if (barcodes.length > 0) {
					console.log('Detected barcodes:', barcodes);
				}
			})
			.catch((error) => {
				console.error('Error detecting barcodes:', error);
			});

		requestAnimationFrame(() => this.scanQRCodeWithBarcodeAPI(video, canvas, context));
	}

	startScannerManually() {
		const video = this.videoElement.nativeElement;
		const canvas = this.canvasElement.nativeElement;

		const context = canvas.getContext('2d', { willReadFrequently: true });

		navigator.mediaDevices
			.getUserMedia({ video: { facingMode: { exact: 'environment' } } })
			.then((stream) => {
				video.srcObject = stream;
				video.play();

				video.addEventListener('loadedmetadata', () => {
					requestAnimationFrame(() => this.scanQRCodeManually(video, canvas, context));
				});
			})
			.catch((error) => {
				console.error('Error accessing camera:', error);
			});
	}

	scanQRCodeManually(video: HTMLVideoElement, canvas: HTMLCanvasElement, context: CanvasRenderingContext2D) {
		// Set canvas dimensions to match the video frame size
		const videoWidth = video.videoWidth;
		const videoHeight = video.videoHeight;
		canvas.width = videoWidth;
		canvas.height = videoHeight;

		// Draw the current video frame onto the canvas
		context.drawImage(video, 0, 0, videoWidth, videoHeight);

		// Extract the image data from the canvas
		const imageData = context.getImageData(0, 0, videoWidth, videoHeight);

		const hints = new Map();
		const formats = [BarcodeFormat.QR_CODE, BarcodeFormat.AZTEC];
		hints.set(DecodeHintType.POSSIBLE_FORMATS, formats);

		const reader = new MultiFormatReader();

		const luminanceSource = new RGBLuminanceSource(imageData.data, videoWidth, videoHeight);
		const binaryBitmap = new BinaryBitmap(new HybridBinarizer(luminanceSource));
		const code = reader.decode(binaryBitmap, hints);
		if (code) {
			console.log({ code });
		}

		requestAnimationFrame(() => this.scanQRCodeManually(video, canvas, context));
	}

	stopScanner() {
		const video = this.videoElement.nativeElement;
		const stream = video.srcObject as MediaStream;

		if (stream) {
			stream.getTracks().forEach((track) => track.stop());
			video.srcObject = null;
		}

		this.scanning = false;
	}

	leaveScanner(): void {
		this.stopScanner();
		this.dialogRef.close();
	}
} */
