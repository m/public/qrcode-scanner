# QR Code Scanner Documentation

---

## Angular Component Implementation

### 1. Setup:

Ensure the following dependencies are installed:

- Angular Material (for dialog and other UI elements)
- jsQR (version 1.4.0)

### 2. Integration:

- Import the `QrCodeScannerComponent` and its corresponding module (`QrCodeScannerModule`) into your Angular module.
- Add the component `<app-qr-code-scanner></app-qr-code-scanner>` where you want the scanner to appear.

### 3. Usage:

- The scanner will automatically start when the component is initialized.
- The decoded QR code content will be displayed in the component.
- You can stop the scanner using the "Stop" button or exit using the "Leave" button.

---

## Vanilla JS Implementation

### 1. Setup:

Include the `jsQR` library in your project:

```html
<script src="https://cdn.jsdelivr.net/npm/jsqr@1.4.0/dist/jsQR.min.js"></script>
```
### 2. Integration:
Include the content of qr-code-scanner.html in your HTML where you want the scanner to appear.

### 3. Usage:
Click the "Start QR Scanner" button to initiate the scanner.
Allow camera access when prompted.
The decoded QR code content will be displayed below the video feed.