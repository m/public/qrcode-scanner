# Documentation du Scanner de QR Code

---

## Mise en œuvre du Composant Angular

### 1. Configuration :

Assurez-vous que les dépendances suivantes sont installées :
- Angular Material (pour les dialogues et d'autres éléments UI)
- jsQR (version 1.4.0)

### 2. Intégration :
- Importez le `QrCodeScannerComponent` et son module correspondant (`QrCodeScannerModule`) dans votre module Angular.
- Ajoutez le composant `<app-qr-code-scanner></app-qr-code-scanner>` là où vous souhaitez que le scanner apparaisse.

### 3. Utilisation :
- Le scanner démarrera automatiquement lorsque le composant sera initialisé.
- Le contenu du QR code décodé sera affiché dans le composant.
- Vous pouvez arrêter le scanner en utilisant le bouton "Arrêt" ou quitter en utilisant le bouton "Quitter".

---

## Mise en œuvre en Vanilla JS

### 1. Configuration :
Incluez la bibliothèque `jsQR` dans votre projet :

```html
<script src="https://cdn.jsdelivr.net/npm/jsqr@1.4.0/dist/jsQR.min.js"></script>
```

### 2. Intégration :
Incluez le contenu de qr-code-scanner.html dans votre HTML là où vous souhaitez que le scanner apparaisse.

### 3. Utilisation :
Cliquez sur le bouton "Démarrer le Scanner QR" pour lancer le scanner.
Autorisez l'accès à la caméra lorsqu'il vous est demandé.
Le contenu du QR code décodé sera affiché sous le flux vidéo.
